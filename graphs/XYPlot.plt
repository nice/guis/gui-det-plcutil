<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title></title>
    <save_changes>true</save_changes>
    <show_legend>true</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>true</grid>
    <scroll>true</scroll>
    <update_period>3.0</update_period>
    <scroll_step>5</scroll_step>
    <start>-1.00 h</start>
    <end>now</end>
    <archive_rescale>NONE</archive_rescale>
    <foreground>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
    </foreground>
    <background>
        <red>229</red>
        <green>229</green>
        <blue>229</blue>
    </background>
    <title_font>Liberation Sans|20|1</title_font>
    <label_font>Liberation Sans|14|1</label_font>
    <scale_font>Liberation Sans|12|0</scale_font>
    <legend_font>Liberation Sans|14|0</legend_font>
    <axes>
        <axis>
            <visible>true</visible>
            <name>Setpoint [SCCM]</name>
            <use_axis_name>true</use_axis_name>
            <use_trace_names>false</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>0.0</min>
            <max>50.0</max>
            <grid>true</grid>
            <autoscale>true</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Mass flow [SCCM]</name>
            <use_axis_name>true</use_axis_name>
            <use_trace_names>false</use_trace_names>
            <right>true</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>0.0</min>
            <max>50.0</max>
            <grid>false</grid>
            <autoscale>true</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Pressure [Bara]</name>
            <use_axis_name>true</use_axis_name>
            <use_trace_names>false</use_trace_names>
            <right>true</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>0.0</min>
            <max>10.0</max>
            <grid>false</grid>
            <autoscale>true</autoscale>
            <log_scale>false</log_scale>
        </axis>
        <axis>
            <visible>true</visible>
            <name>Temperature [C]</name>
            <use_axis_name>true</use_axis_name>
            <use_trace_names>false</use_trace_names>
            <right>true</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>0.0</min>
            <max>50.0</max>
            <grid>false</grid>
            <autoscale>true</autoscale>
            <log_scale>false</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>$(DEV):al_ctlr_sccm</display_name>
            <visible>true</visible>
            <name>$(DEV):al_ctlr_sccm</name>
            <axis>1</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):al_mt_sccm_sccm</display_name>
            <visible>true</visible>
            <name>$(DEV):al_mt_sccm_sccm</name>
            <axis>1</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):al_ctlr_pressure</display_name>
            <visible>true</visible>
            <name>$(DEV):al_ctlr_pressure</name>
            <axis>2</axis>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):al_mt_pressure</display_name>
            <visible>true</visible>
            <name>$(DEV):al_mt_pressure</name>
            <axis>2</axis>
            <color>
                <red>255</red>
                <green>127</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):pressure_01</display_name>
            <visible>true</visible>
            <name>$(DEV):pressure_01</name>
            <axis>2</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>127</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):al_ctlr_temperature</display_name>
            <visible>true</visible>
            <name>$(DEV):al_ctlr_temperature</name>
            <axis>3</axis>
            <color>
                <red>127</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):al_mt_temperature</display_name>
            <visible>true</visible>
            <name>$(DEV):al_mt_temperature</name>
            <axis>3</axis>
            <color>
                <red>255</red>
                <green>255</green>
                <blue>0</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):temperature_01</display_name>
            <visible>true</visible>
            <name>$(DEV):temperature_01</name>
            <axis>3</axis>
            <color>
                <red>0</red>
                <green>255</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
        <pv>
            <display_name>$(DEV):al_ctlr_sp</display_name>
            <visible>true</visible>
            <name>$(DEV):al_ctlr_sp</name>
            <axis>0</axis>
            <color>
                <red>255</red>
                <green>0</green>
                <blue>255</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <line_style>SOLID</line_style>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>RAW</request>
            <archive>
                <name>ESS Archiver</name>
                <url>pbraw://archiver-01.tn.esss.lu.se:17668/retrieval</url>
                <key>1</key>
            </archive>
        </pv>
    </pvlist>
</databrowser>